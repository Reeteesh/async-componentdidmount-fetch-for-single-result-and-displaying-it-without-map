import React, { Component } from "react";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      person: "",
    };
  }
  async componentDidMount() {
    const url = "https://api.randomuser.me/";
    const response = await fetch(url);
    const data = await response.json();
    console.log(data.results);
    this.setState({
      person: data.results[0],
      loading: false,
    });
  }
  render() {
    if (this.state.loading) {
      return <div>loading....</div>;
    }
    if (!this.state.person) {
      return <div>didn't get a person</div>;
    }
    return (
      <>
        Name:
        {this.state.person.name.title}
        {this.state.person.name.first}
        {this.state.person.name.last}
        Age:{this.state.person.gender}
      </>
    );
  }
}

export default App;
